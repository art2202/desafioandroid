## Desafio Desenvolvedor Mobile Android
Iplanrio - Empresa Municipal de Informática do RJ

## O que foi feito:

+ ### Funcionalidades:
    * - Tela de mapa para mostrar rotas pesquisadas
    * - Tela com histórico das corridas (com dados da api mock)
    * - funcionamento offline das rotas ja pesquisadas
#
* * *
+ ## Sobre o projeto
    * - Arquitetura geral: Clean Architecture
    * - Arquitetura da camada de apresentação: MVVM
    * - Injeção de dependencia com Koin
    * - Requisição com Retrofit 2.9.0
    * - Assincronicidade com Kotlin Coroutines
    * - Maps API para exibição do mapa
    * - Directions API para traçar rota, exibir tempo e distancia
#
* * *
* ## Bibliotecas utilizadas        
    * - Retrofit 2.6.0 - Para interface da API
    * - Gson - Para conversão de Json para objetos
    * - Kotlin Coroutines - Programar assincronamente, principalmente na hora de fazer requisiçõees
    * - Kotlin Koin - Para injeção de dependência
    * - LifeCycle - Para controle de ciclo de vida do viewModel
    * - Room Database - Para armazenamento de dados no dispositivo(Escolhido pela facilidade da implementação)
    
        
## Contato
* linkedin : https://www.linkedin.com/in/arthur-rodrigues-837373184/
* E-mail : arthurrodrigues29@gmail.com    

    