package com.example.desafioinplanrio.data.mapper

import com.example.desafioinplanrio.core.utils.Constantes
import com.example.desafioinplanrio.data.database.entity.EnderecoEntity
import com.example.desafioinplanrio.data.database.entity.SegmentoEntity
import com.example.desafioinplanrio.data.database.entity.ViagemEntity
import com.example.desafioinplanrio.domain.entities.Viagem
import java.text.SimpleDateFormat
import java.util.*


//criado por arthur rodrigues

class ViagemToTripleMapper : Mapper<Viagem, Triple<ViagemEntity, EnderecoEntity, List<SegmentoEntity>>> {
    override fun map(input: Viagem): Triple<ViagemEntity, EnderecoEntity, List<SegmentoEntity>> {
        val sdf = SimpleDateFormat(Constantes.FORMATO_DATA_ISO, Locale.US)
        val date = sdf.format(Date())

        val viagemEntity = ViagemEntity(
            date,
            input.enderecoInicial.endereco,
            input.enderecoFinal.endereco,
            input.duracao,
            input.distancia
        )

        val enderecoEntity = EnderecoEntity(
            null,
            input.enderecoInicial.endereco,
            input.enderecoInicial.latLng.latitude,
            input.enderecoInicial.latLng.longitude,
            input.enderecoFinal.endereco,
            input.enderecoFinal.latLng.latitude,
            input.enderecoFinal.latLng.longitude,
            date
        )

        val segmentosEntities = input.rota.map {
            SegmentoEntity(
                null,
                it.enderecoInicial.latitude,
                it.enderecoInicial.longitude,
                it.enderecoFinal.latitude,
                it.enderecoFinal.longitude,
                date
            )
        }
        return Triple(viagemEntity, enderecoEntity, segmentosEntities)
    }
}