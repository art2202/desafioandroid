package com.example.desafioinplanrio.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.desafioinplanrio.data.database.entity.SegmentoEntity

//criado por arthur rodrigues

@Dao
interface SegmentoDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun add(segmento : List<SegmentoEntity>)

    @Query("SELECT * FROM segmento WHERE data_viagem = :dataViagem")
    fun getSegmentos(dataViagem : String?) : List<SegmentoEntity>?
}