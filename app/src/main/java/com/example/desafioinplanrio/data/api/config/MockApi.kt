package com.example.desafioinplanrio.data.api.config

import com.example.desafioinplanrio.core.utils.Constantes
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object MockApi {

    private val retrofit : Retrofit
    fun getRetrofit() = retrofit

    init {

        retrofit = Retrofit.Builder()
            .baseUrl(Constantes.MOCK_URL_API)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
    }

}