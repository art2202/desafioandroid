package com.example.desafioinplanrio.data.mapper


//criado por arthur rodrigues

interface Mapper<I, O> {

    fun map(input : I) : O
}