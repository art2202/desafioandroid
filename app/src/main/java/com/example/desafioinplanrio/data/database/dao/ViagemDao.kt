package com.example.desafioinplanrio.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.desafioinplanrio.data.database.entity.ViagemEntity


//criado por arthur rodrigues

@Dao
interface ViagemDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun add(viagem : List<ViagemEntity>)

    @Query("SELECT * FROM viagem")
    fun getAll() : List<ViagemEntity>

    @Query("SELECT * FROM viagem WHERE endereco_origem LIKE '%' || :origem || '%' AND endereco_destino LIKE '%' || :destino || '%' ")
    fun getOneViagem(origem : String, destino : String) : ViagemEntity?
}