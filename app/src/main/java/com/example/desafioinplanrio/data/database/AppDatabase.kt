package com.example.desafioinplanrio.data.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.desafioinplanrio.core.App
import com.example.desafioinplanrio.data.database.dao.EnderecoDao
import com.example.desafioinplanrio.data.database.dao.SegmentoDao
import com.example.desafioinplanrio.data.database.dao.ViagemDao
import com.example.desafioinplanrio.data.database.entity.EnderecoEntity
import com.example.desafioinplanrio.data.database.entity.SegmentoEntity
import com.example.desafioinplanrio.data.database.entity.ViagemEntity


@Database(

    version = 1,
    exportSchema = false,
    entities = [
        ViagemEntity::class,
        SegmentoEntity::class,
        EnderecoEntity::class
    ]
)

abstract class AppDatabase : RoomDatabase() {

    abstract fun ViagemDao() : ViagemDao
    abstract fun segmentoDao() : SegmentoDao
    abstract fun enderecoDao() : EnderecoDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase (): AppDatabase? {
            if (this.INSTANCE != null) {
                return this.INSTANCE
            } else {
                synchronized(this) {
                    val instance = Room.databaseBuilder(
                        App.instance,
                        AppDatabase::class.java,
                        "challenger-inplanrio")
                        .build()
                    this.INSTANCE = instance
                    return instance
                }
            }
        }
    }
}