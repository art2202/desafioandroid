package com.example.desafioinplanrio.data.database.entity


//criado por arthur rodrigues

enum class TipoEndereco {

    ORIGEM, DESTINO
}