package com.example.desafioinplanrio.data.data_source

import com.example.desafioinplanrio.data.models.GoogleResponse
import com.example.desafioinplanrio.data.models.MockResponse


//criado por arthur rodrigues

interface RouteRemoteDataSource {

    suspend fun getRoute(origem : String, destino : String) : GoogleResponse

    suspend fun getMockResponse() : MockResponse
}