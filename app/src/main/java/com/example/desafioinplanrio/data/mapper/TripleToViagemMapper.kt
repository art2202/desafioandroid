package com.example.desafioinplanrio.data.mapper

import com.example.desafioinplanrio.data.database.entity.EnderecoEntity
import com.example.desafioinplanrio.data.database.entity.SegmentoEntity
import com.example.desafioinplanrio.data.database.entity.ViagemEntity
import com.example.desafioinplanrio.domain.entities.Endereco
import com.example.desafioinplanrio.domain.entities.Segmento
import com.example.desafioinplanrio.domain.entities.Viagem
import com.google.android.gms.maps.model.LatLng


//criado por arthur rodrigues

class TripleToViagemMapper : Mapper<Triple<ViagemEntity?, EnderecoEntity?, List<SegmentoEntity>?>, Viagem>{
    override fun map(input: Triple<ViagemEntity?, EnderecoEntity?, List<SegmentoEntity>?>): Viagem {
        return Viagem(
            input.first!!.dataViagem,
            input.first!!.distancia,
            input.first!!.tempo,
            Endereco(input.second!!.textEnderecoInicio, LatLng(input.second!!.latInicio, input.second!!.lngInicio)),
            Endereco(input.second!!.textEnderecoFim, LatLng(input.second!!.latFim, input.second!!.lngFim)),
            input.third!!.map {
                Segmento(
                    LatLng(it.latitudeInicio, it.longitudeInicio),
                    LatLng(it.latitudeFim, it.longitudeFim)
                )
            }
        )
    }
}