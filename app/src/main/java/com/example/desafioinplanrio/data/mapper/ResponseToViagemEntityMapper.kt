package com.example.desafioinplanrio.data.mapper

import com.example.desafioinplanrio.core.utils.Constantes
import com.example.desafioinplanrio.data.database.entity.ViagemEntity
import com.example.desafioinplanrio.data.models.GoogleResponse
import java.text.SimpleDateFormat
import java.util.*


//criado por arthur rodrigues

class ResponseToViagemEntityMapper : Mapper<GoogleResponse, List<ViagemEntity> >{
    override fun map(input: GoogleResponse): List<ViagemEntity> {
        val sdf = SimpleDateFormat(Constantes.FORMATO_DATA_ISO, Locale.US)
        val date = sdf.format(Date())
        return input.routes.map {
            it.legs.map {
                ViagemEntity(
                    date,
                    it.start_address,
                    it.end_address,
                    it.duration.text,
                    it.distance.text
                )
            }
        }.flatten()
    }
}