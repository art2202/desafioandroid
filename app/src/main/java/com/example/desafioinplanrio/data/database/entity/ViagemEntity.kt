package com.example.desafioinplanrio.data.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


//criado por arthur rodrigues

@Entity(
    tableName = "viagem"
)
data class ViagemEntity(

    @PrimaryKey
    @ColumnInfo(name = "data_viagem")
    val dataViagem : String,

    @ColumnInfo(name = "endereco_origem")
    val enderecoOrigem : String,

    @ColumnInfo(name = "endereco_destino")
    val enderecoDestino : String,

    val tempo : String,

    val distancia : String

)