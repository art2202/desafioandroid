package com.example.desafioinplanrio.data.mapper

import com.example.desafioinplanrio.data.database.entity.SegmentoEntity
import com.example.desafioinplanrio.data.models.GoogleResponse

//criado por arthur rodrigues

class ResponseToSegmentoEntityMapper : Mapper<Pair<GoogleResponse, String>, List<SegmentoEntity>>{
    override fun map(input: Pair<GoogleResponse, String>): List<SegmentoEntity> {
        return input.first.routes.map {
            it.legs.map {
                it.steps.map {
                    SegmentoEntity(
                        null,
                        it.start_location.lat,
                        it.start_location.lng,
                        it.end_location.lat,
                        it.end_location.lng,
                        input.second
                    )
                }
            }.flatten()
        }.flatten()
    }
}