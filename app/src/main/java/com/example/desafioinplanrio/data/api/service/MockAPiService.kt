package com.example.desafioinplanrio.data.api.service

import com.example.desafioinplanrio.data.models.MockResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url


//criado por arthur rodrigues

interface MockAPiService {

    @GET
    suspend fun getDadosMock(@Url url : String = "routes") : Response<MockResponse>
}