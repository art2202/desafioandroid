package com.example.desafioinplanrio.data.api.config

import com.example.desafioinplanrio.core.utils.Constantes
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

//criado por arthur rodrigues

object RouteApi {

    private val retrofit : Retrofit
    fun getRetrofit() = retrofit

    init {
//        "https://api.nytimes.com/svc/movies/v2/"
//        "https://maps.googleapis.com/maps/api/directions/json?"
      retrofit = Retrofit.Builder()
            .baseUrl(Constantes.DIRECTION_URL_API)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
    }


}