package com.example.desafioinplanrio.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.desafioinplanrio.data.database.entity.EnderecoEntity


//criado por arthur rodrigues

@Dao
interface EnderecoDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun add(endereco : List<EnderecoEntity>)

    @Query("SELECT * FROM endereco WHERE data_viagem = :dataViagem")
    fun getEnderecos(dataViagem : String?) : EnderecoEntity?

}