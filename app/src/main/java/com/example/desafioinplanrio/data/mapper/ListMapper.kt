package com.example.desafioinplanrio.data.mapper


//criado por arthur rodrigues

interface ListMapper<I, O> : Mapper<List<I>, List<O>>