package com.example.desafioinplanrio.data.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey


//criado por arthur rodrigues
@Entity(
    tableName = "endereco",
    foreignKeys = [
        ForeignKey(
            entity = ViagemEntity::class,
            parentColumns = arrayOf("data_viagem"),
            childColumns = arrayOf("data_viagem")
        )]
)
data class EnderecoEntity(

    @PrimaryKey(autoGenerate = true)
    val id : Int?,

    @ColumnInfo(name = "text_endereco_inicio")
    val textEnderecoInicio : String,

    @ColumnInfo(name = "latitude_inicio")
    val latInicio : Double,

    @ColumnInfo(name = "longitude_inicio")
    val lngInicio : Double,

    @ColumnInfo(name = "text_endereco_fim")
    val textEnderecoFim: String,

    @ColumnInfo(name = "latitude_fim")
    val latFim : Double,

    @ColumnInfo(name = "longitude_fim")
    val lngFim : Double,

    @ColumnInfo(name = "data_viagem", index = true)
    val data_viagem : String
)