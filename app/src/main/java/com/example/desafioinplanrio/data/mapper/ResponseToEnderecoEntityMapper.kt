package com.example.desafioinplanrio.data.mapper

import com.example.desafioinplanrio.data.database.entity.EnderecoEntity
import com.example.desafioinplanrio.data.models.GoogleResponse


//criado por arthur rodrigues

class ResponseToEnderecoEntityMapper : Mapper<Pair<GoogleResponse, String>, List<EnderecoEntity> > {
    override fun map(input: Pair<GoogleResponse, String>): List<EnderecoEntity> {
        return input.first.routes.map {
            it.legs.map{
                EnderecoEntity(
                    null,
                    it.start_address,
                    it.start_location.lat,
                    it.start_location.lng,
                    it.end_address,
                    it.end_location.lat,
                    it.end_location.lng,
                    input.second
                )
            }
        }.flatten()
    }
}