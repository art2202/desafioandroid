package com.example.desafioinplanrio.data.data_source


import com.example.desafioinplanrio.core.utils.Constantes
import com.example.desafioinplanrio.core.exceptions.InvalidApiKeyThrowable
import com.example.desafioinplanrio.core.exceptions.ResourceNotFoundThrowable
import com.example.desafioinplanrio.data.api.service.MockAPiService
import com.example.desafioinplanrio.data.api.service.RouteAPiService
import com.example.desafioinplanrio.data.models.GoogleResponse
import com.example.desafioinplanrio.data.models.MockResponse


//criado por arthur rodrigues

class RouteRemoteDataSourceImpl(private val directionsApi : RouteAPiService,
                                private val mockApi : MockAPiService) : RouteRemoteDataSource {

    override suspend fun getRoute(origem: String, destino: String): GoogleResponse {

        val url = "json?origin=$origem&destination=$destino&key=${Constantes.API_TOKEN}"
        val response = directionsApi.getRoute(url)
        when {
            response.isSuccessful -> {
                if (response.body()!!.status == "NOT_FOUND")
                    throw ResourceNotFoundThrowable()

                else
                    return response.body()!!
            }

            response.code() == 401 -> {
                throw InvalidApiKeyThrowable()
            }
            response.code() == 404 -> {
                throw ResourceNotFoundThrowable()
            }
            else -> throw  Throwable()
        }
    }

    override suspend fun getMockResponse(): MockResponse {
        val response = mockApi.getDadosMock()
        when {
            response.isSuccessful -> {
                return response.body()!!
            }
            response.code() == 401 -> {
                throw InvalidApiKeyThrowable()
            }
            response.code() == 404 -> {
                throw ResourceNotFoundThrowable()
            }
            else -> throw  Throwable()
        }
    }
}