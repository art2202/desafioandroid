package com.example.desafioinplanrio.data.data_source

import com.example.desafioinplanrio.data.database.entity.EnderecoEntity
import com.example.desafioinplanrio.data.database.entity.SegmentoEntity
import com.example.desafioinplanrio.data.database.entity.ViagemEntity


//criado por arthur rodrigues

interface RouteLocalDataSource {

    suspend fun salvaViagem(viagens : List<ViagemEntity>)

    suspend fun salvaEnderecos(enderecos: List<EnderecoEntity>)

    suspend fun salvaSegmentos(segmentos: List<SegmentoEntity>)

    suspend fun getOneViagem(origem : String, destino : String) : Triple<ViagemEntity?, EnderecoEntity?, List<SegmentoEntity>?>

    suspend fun getViagens() : Triple<List<ViagemEntity>, List<EnderecoEntity>, List<SegmentoEntity>>

}