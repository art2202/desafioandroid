package com.example.desafioinplanrio.data.mapper

import com.example.desafioinplanrio.data.models.MockResponse
import com.example.desafioinplanrio.domain.entities.SimpleViagem


//criado por arthur rodrigues

class MockResponseToSimpleViagemMapper : Mapper<MockResponse, List<SimpleViagem>> {
    override fun map(input: MockResponse): List<SimpleViagem> {
        return input.viagens.map {
            SimpleViagem(
                it.origem,
                it.destino,
                it.dataSolicitacao,
                it.distancia,
                it.tempo
            )
        }
    }
}