package com.example.desafioinplanrio.data.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey


//criado por arthur rodrigues

@Entity(
    tableName = "segmento",
    foreignKeys = [
        ForeignKey(
            entity = ViagemEntity::class,
            parentColumns = arrayOf("data_viagem"),
            childColumns = arrayOf("data_viagem")
        )]
)
data class SegmentoEntity(

    @PrimaryKey(autoGenerate = true)
    val id : Int?,

    @ColumnInfo(name = "latitude_inicio")
    val latitudeInicio : Double,

    @ColumnInfo(name = "longitude_inicio")
    val longitudeInicio : Double,

    @ColumnInfo(name = "latitude_fim")
    val latitudeFim : Double,

    @ColumnInfo(name = "longitude_fim")
    val longitudeFim : Double,

    @ColumnInfo(name = "data_viagem", index = true)
    val data_viagem : String
)