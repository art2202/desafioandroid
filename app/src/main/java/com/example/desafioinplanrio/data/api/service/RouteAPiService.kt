package com.example.desafioinplanrio.data.api.service

import com.example.desafioinplanrio.data.models.GoogleResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url


//criado por arthur rodrigues

interface RouteAPiService {

    @GET
    suspend fun getRoute(@Url url : String) : Response<GoogleResponse>
}