package com.example.desafioinplanrio.data.repositories

import com.example.desafioinplanrio.data.data_source.RouteLocalDataSource
import com.example.desafioinplanrio.data.data_source.RouteRemoteDataSource
import com.example.desafioinplanrio.data.database.entity.EnderecoEntity
import com.example.desafioinplanrio.data.database.entity.SegmentoEntity
import com.example.desafioinplanrio.data.mapper.*
import com.example.desafioinplanrio.data.models.GoogleResponse
import com.example.desafioinplanrio.domain.entities.Endereco
import com.example.desafioinplanrio.domain.entities.SimpleViagem
import com.example.desafioinplanrio.domain.entities.Viagem
import com.example.desafioinplanrio.domain.repositories.RouteRepository


//criado por arthur rodrigues

class RouteRepositoryImpl(private val remoteDataSource: RouteRemoteDataSource,
                          private val responseToViagemEntityMapper: ResponseToViagemEntityMapper,
                          private val responseToEnderecoEntityMapper: ResponseToEnderecoEntityMapper,
                          private val responseToSegmentoEntityMapper: ResponseToSegmentoEntityMapper,
                          private val tripleToViagemMapper: TripleToViagemMapper,
                          private val mockResponseToSimpleViagemMapper: MockResponseToSimpleViagemMapper,
                          private val localDataSource: RouteLocalDataSource) : RouteRepository {

    override suspend fun getRoute(origem: String, destino: String): Viagem {

        val rotaExistente = getLocalRoute(origem, destino)

        if(rotaExistente == null) {
            val response = remoteDataSource.getRoute(origem, destino)
            salvaDadosRota(response)
            return getLocalRoute(origem, destino)!!
        }
        else{
            return rotaExistente
        }
//        else{
//            val tripleEntity = viagemToTripleMapper.map(rotaExistente)
//            localDataSource.salvaViagem(listOf(tripleEntity.first))
//            localDataSource.salvaEnderecos(listOf(tripleEntity.second))
//            localDataSource.salvaSegmentos(tripleEntity.third)
//        }

//        return getLocalRoute(origem, destino)!!
    }

    override suspend fun salvaDadosRota(response: GoogleResponse) {
        val viagensEntities = responseToViagemEntityMapper.map(response)

        val enderecos = arrayListOf<EnderecoEntity>()
        val segmentos = arrayListOf<SegmentoEntity>()

        viagensEntities.map {
                enderecos.addAll(responseToEnderecoEntityMapper.map(Pair(response, it.dataViagem)))
                segmentos.addAll(responseToSegmentoEntityMapper.map(Pair(response, it.dataViagem)))
        }

        localDataSource.salvaViagem(viagensEntities)
        localDataSource.salvaEnderecos(enderecos)
        localDataSource.salvaSegmentos(segmentos)
    }

    override suspend fun getLocalRoute(origem: String, destino: String): Viagem? {
        val tabelasBanco = localDataSource.getOneViagem(origem, destino)
        if(tabelasBanco.first != null && tabelasBanco.second != null && tabelasBanco.third != null){
            return tripleToViagemMapper.map(tabelasBanco)
        }
        else
            return null


    }

    override suspend fun getHistorico() : List<SimpleViagem> {
        val mockResponse = remoteDataSource.getMockResponse()
        return mockResponseToSimpleViagemMapper.map(mockResponse)
    }
}