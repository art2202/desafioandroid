package com.example.desafioinplanrio.data.data_source

import com.example.desafioinplanrio.data.database.AppDatabase
import com.example.desafioinplanrio.data.database.entity.EnderecoEntity
import com.example.desafioinplanrio.data.database.entity.SegmentoEntity
import com.example.desafioinplanrio.data.database.entity.ViagemEntity


//criado por arthur rodrigues

class RouteLocalDataSourceImpl : RouteLocalDataSource {

    private val viagemDao = AppDatabase.getDatabase()!!.ViagemDao()
    private val enderecoDao = AppDatabase.getDatabase()!!.enderecoDao()
    private val segmentoDao = AppDatabase.getDatabase()!!.segmentoDao()

    override suspend fun salvaViagem(viagens: List<ViagemEntity>) {
        viagemDao.add(viagens)
    }

    override suspend fun salvaEnderecos(enderecos: List<EnderecoEntity>) {
        enderecoDao.add(enderecos)
    }

    override suspend fun salvaSegmentos(segmentos: List<SegmentoEntity>) {
        segmentoDao.add(segmentos)
    }

    override suspend fun getOneViagem(origem: String,
                                      destino: String): Triple<ViagemEntity?, EnderecoEntity?, List<SegmentoEntity>?> {

        val viagemEntity = viagemDao.getOneViagem(origem, destino)
        val enderecosEntity = enderecoDao.getEnderecos(viagemEntity?.dataViagem)
        val segmentosEntity = segmentoDao.getSegmentos(viagemEntity?.dataViagem)

        return Triple(viagemEntity, enderecosEntity, segmentosEntity)

    }

    override suspend fun getViagens(): Triple<List<ViagemEntity>, List<EnderecoEntity>, List<SegmentoEntity>> {
        val viagensEntities = viagemDao.getAll()
        val enderecosEntities = arrayListOf<EnderecoEntity>()
        val segmentosEntity = arrayListOf<SegmentoEntity>()

        viagensEntities.map {
            val endereco = enderecoDao.getEnderecos(it.dataViagem)
            val segmento = segmentoDao.getSegmentos(it.dataViagem)
            if(endereco != null && segmento != null) {
                enderecosEntities.add(endereco)
                segmentosEntity.addAll(segmento)
            }
        }

        return Triple(viagensEntities, enderecosEntities, segmentosEntity)
    }
}