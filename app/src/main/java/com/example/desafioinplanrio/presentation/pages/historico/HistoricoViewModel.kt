package com.example.desafioinplanrio.presentation.pages.historico

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.challengeyourdev.core.utils.Response
import com.example.desafioinplanrio.domain.usecases.GetHistorico
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


//criado por arthur rodrigues

class HistoricoViewModel(private val getHistoricoUseCase: GetHistorico) : ViewModel() {

    private val response = MutableLiveData<Response>()

    fun getHistorico(){
        CoroutineScope(Dispatchers.IO).launch {
            response.postValue(Response.loading())
            try {
                val result = getHistoricoUseCase()
                response.postValue(Response.success(result))
            }
            catch (t : Throwable){
                response.postValue(Response.error(t))
            }
        }
    }

    fun response(): MutableLiveData<Response> {
        return response
    }
}