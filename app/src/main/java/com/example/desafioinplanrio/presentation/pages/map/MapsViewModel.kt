package com.example.desafioinplanrio.presentation.pages.map

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.challengeyourdev.core.utils.Response
import com.example.desafioinplanrio.data.database.entity.TipoEndereco
import com.example.desafioinplanrio.domain.entities.Viagem
import com.example.desafioinplanrio.domain.usecases.GetRoute
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


//criado por arthur rodrigues

class MapsViewModel(private val getRouteUseCase : GetRoute) : ViewModel() {
    private val response = MutableLiveData<Response>()

    fun getRoute(origem : String, destino : String){
        response.postValue(Response.loading())
        CoroutineScope(Dispatchers.IO).launch {
            response.postValue(Response.loading())
            try {
                val result = getRouteUseCase(origem, destino)
                response.postValue(Response.success(result))
            }
            catch (t : Throwable) {
                response.postValue(Response.error(t))
            }
        }
    }


    fun response(): MutableLiveData<Response> {
        return response
    }
}