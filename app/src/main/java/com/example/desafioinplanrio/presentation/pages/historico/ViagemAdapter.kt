package com.example.desafioinplanrio.presentation.pages.historico

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.desafioinplanrio.R
import com.example.desafioinplanrio.domain.entities.SimpleViagem
import kotlinx.android.synthetic.main.item_viagem.view.*


//criado por arthur rodrigues

class ViagemAdapter(private val viagens : List<SimpleViagem>) : RecyclerView.Adapter<ViagemAdapter.MyViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_viagem, parent, false))
    }

    override fun getItemCount(): Int = viagens.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = viagens[position]

        holder.itemView.text_origem.text = "origem: ${item.origem}"
        holder.itemView.text_destino.text = "destino: ${item.destino}"
        holder.itemView.text_data.text = "data: ${item.dataSolicitacao}"
        holder.itemView.text_distancia.text = "distancia: ${item.distancia}"
        holder.itemView.text_tempo.text = "tempo: ${item.tempo}"
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)
}
