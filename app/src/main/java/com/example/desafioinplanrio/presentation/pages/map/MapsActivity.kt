package com.example.desafioinplanrio.presentation.pages.map

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import com.example.challengeyourdev.core.utils.Response
import com.example.challengeyourdev.core.utils.Status
import com.example.desafioinplanrio.R
import com.example.desafioinplanrio.domain.entities.Viagem
import com.example.desafioinplanrio.presentation.pages.historico.HistoricoActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.activity_maps.*
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import org.koin.androidx.viewmodel.ext.android.viewModel


@SuppressLint("MissingPermission")
class MapsActivity : AppCompatActivity(), OnMapReadyCallback,  GoogleMap.OnMarkerClickListener {

    private lateinit var mMap: GoogleMap
    private var lastLocation: Location? = null
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var lastcurrentLocalization: LatLng? = null
    private val LOCATION_PERMISSION_REQUEST_CODE = 1

    private val viewModel : MapsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        viewModel.response().observe(this, Observer { response -> processResponse(response) })

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        getLocalizacao()

        botao_historico.setOnClickListener {
            intent = Intent(this, HistoricoActivity::class.java)
            startActivity(intent)
        }
        limpar.setOnClickListener {
            limparEnderecos()
        }

        salvar.setOnClickListener {
            val origem = edit_text_origem.text.toString()
            val destino = edit_text_destino.text.toString()
            if(origem.isNotEmpty() &&  destino.isNotEmpty()) {


                viewModel.getRoute(origem, destino)
                input_layout_origem.isErrorEnabled = false
                input_layout_destino.isErrorEnabled = false
            }
            else{
                if(origem.isEmpty())
                    input_layout_origem.error = "Insira um endereço de partida"
                else
                    input_layout_origem.isErrorEnabled = false

                if(destino.isEmpty())
                    input_layout_destino.error = "Insira um endereço de destino"
                else
                    input_layout_destino.isErrorEnabled = false
            }

        }
        btnGps.setOnClickListener { centralizarLocalizacao() }

        if (ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1)
        }
        else getLocalizacao()
    }

    private fun limparEnderecos() {
        edit_text_origem.setText("")
        edit_text_destino.setText("")
    }

    private fun processResponse(response : Response){

        when (response.status) {
            Status.LOADING -> showLoading()
            Status.SUCCESS -> tracaRota(response.data)
            Status.ERROR -> showError(response.error)
        }
    }

    private fun showLoading(){
        salvar.setBackgroundResource(R.color.Grey2)
        salvar.text = "Salvando"
        text_tempo.visibility = View.INVISIBLE
        text_distancia.visibility = View.INVISIBLE
    }
    private fun tracaRota(data: Any?) {

        mMap.clear()

        salvar.setBackgroundResource(R.color.azul)
        salvar.text = "Salvar"
        val viagem = data as Viagem

        mMap.addMarker(MarkerOptions().position(viagem.enderecoInicial.latLng))
        mMap.addMarker(MarkerOptions().position(viagem.enderecoFinal.latLng))

        text_tempo.visibility = View.VISIBLE
        text_distancia.visibility = View.VISIBLE
        text_distancia.text = viagem.distancia
        text_tempo.text = viagem.duracao

        centralizarLocalizacao(viagem.enderecoInicial.latLng)
        viagem.rota.map {
            val listaLatLng = arrayListOf<LatLng>(it.enderecoInicial, it.enderecoFinal)
            mMap.addPolyline(PolylineOptions().clickable(true).addAll(listaLatLng))
            it.enderecoFinal }


    }

    private fun showError(error: Throwable?) {
        salvar.setBackgroundResource(R.color.azul)
        salvar.text = "Salvar"
        Toast.makeText(this, error!!.message, Toast.LENGTH_LONG).show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    getLocalizacao()
                } else throw Exception("Permissões não consedidas")
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        centralizarLocalizacao()

        layers_map.setOnClickListener {
            tipo_mapa.isVisible = !tipo_mapa.isVisible

            if (tipo_mapa.isVisible) {

                tipo_terreno.setOnClickListener {
                    googleMap.mapType = GoogleMap.MAP_TYPE_TERRAIN
                    tipo_terreno.setImageResource(R.drawable.ic_terrain_selecionado)
                    mapa_type.setImageResource(R.drawable.ic_map)
                    tipo_satelite.setImageResource(R.drawable.ic_satellite)
                }

                tipo_satelite.setOnClickListener {
                    googleMap.mapType = GoogleMap.MAP_TYPE_SATELLITE
                    tipo_satelite.setImageResource(R.drawable.ic_satellite_selecionado)
                    mapa_type.setImageResource(R.drawable.ic_map)
                    tipo_terreno.setImageResource(R.drawable.ic_terrain)
                }

                mapa_type.setOnClickListener {
                    googleMap.mapType = GoogleMap.MAP_TYPE_NORMAL
                    mapa_type.setImageResource(R.drawable.ic_map_selecionado)
                    tipo_satelite.setImageResource(R.drawable.ic_satellite)
                    tipo_terreno.setImageResource(R.drawable.ic_terrain)
                }

                transito.setOnClickListener {
                    googleMap.isTrafficEnabled = !googleMap.isTrafficEnabled

                    if (googleMap.isTrafficEnabled)
                        transito.setImageResource(R.drawable.ic_traffic_selecionado)
                    else
                        transito.setImageResource(R.drawable.ic_traffic)
                }
            }
        }
    }

    private fun getLocalizacao() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationClient.lastLocation?.addOnSuccessListener(this) { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {
                lastLocation = location
                lastcurrentLocalization = LatLng(location.latitude, location.longitude)
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lastcurrentLocalization, 15f))
            }
        }
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment

        mapFragment.getMapAsync(this)
    }

    fun centralizarLocalizacao(localizacao : LatLng? = null) {

        if (lastLocation != null) {

            mMap.isMyLocationEnabled = true
            mMap.uiSettings.isMyLocationButtonEnabled = false

            if(localizacao == null) {
                mMap.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(lastLocation!!.latitude, lastLocation!!.longitude), 15f
                    )
                )
            }
            else{
                mMap.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(localizacao.latitude, localizacao.longitude), 15f
                    )
                )
            }
        } else
            getLocalizacao()
    }

    override fun onMarkerClick(p0: Marker?) = false
}
