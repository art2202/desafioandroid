package com.example.desafioinplanrio.presentation.pages.historico

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.challengeyourdev.core.utils.Response
import com.example.challengeyourdev.core.utils.Status
import com.example.desafioinplanrio.R
import com.example.desafioinplanrio.domain.entities.SimpleViagem
import kotlinx.android.synthetic.main.activity_historico.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class HistoricoActivity : AppCompatActivity() {

    private val viewModel : HistoricoViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_historico)

        viewModel.response().observe(this, Observer { response -> processResponse(response) })
        ll_error.visibility = View.INVISIBLE
        viewModel.getHistorico()

        getHistorico()
        
        ll_error.setOnClickListener {
            getHistorico()
        }
        botao_voltar.setOnClickListener {
            onBackPressed()
        }
    }
    private fun getHistorico(){
        viewModel.getHistorico()
    }

    private fun processResponse(response : Response){
        when (response.status) {
            Status.LOADING -> showLoading()
            Status.SUCCESS -> responseSucess(response.data)
            Status.ERROR -> showError(response.error)
        }
    }

    private fun responseSucess(data : Any?){
        pg_loading.visibility = View.GONE
        rv_viagens.visibility = View.VISIBLE
        if(data is List<*>){
            val viagens = data as List<SimpleViagem>
            val adapter = ViagemAdapter(viagens)
            val layoutManager = LinearLayoutManager(this)
            rv_viagens.layoutManager = layoutManager
            rv_viagens.adapter = adapter
        }
    }
    private fun showLoading(){
        pg_loading.visibility = View.VISIBLE
    }


    private fun showError(error: Throwable?) {
        pg_loading.visibility = View.INVISIBLE
        ll_error.visibility = View.VISIBLE
        Toast.makeText(this, error.toString(), Toast.LENGTH_SHORT).show()

    }

}