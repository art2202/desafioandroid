package com.example.desafioinplanrio.core.exceptions

import java.io.IOException

class NoConnectivityException: IOException("Sem conexão com a internet")