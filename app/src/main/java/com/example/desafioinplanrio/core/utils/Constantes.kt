package com.example.desafioinplanrio.core.utils

object Constantes {

    const val DIRECTION_URL_API = "https://maps.googleapis.com/maps/api/directions/"
    const val MOCK_URL_API = "https://private-dc421c-apiroutesart.apiary-mock.com/"
    const val API_TOKEN = "AIzaSyBkg-amFuaX7NHk4F22-enx-abtiwaD8Oc"
    const val FORMATO_DATA_ISO = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
}