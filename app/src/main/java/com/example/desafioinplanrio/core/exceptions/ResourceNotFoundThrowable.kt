package com.example.desafioinplanrio.core.exceptions

class ResourceNotFoundThrowable: Throwable("Rota não encontrada")