package com.example.desafioinplanrio.core.exceptions

import java.lang.Exception

class InvalidApiKeyThrowable: Throwable("Chave de API inválida")