package com.example.desafioinplanrio.core.utils

import com.example.desafioinplanrio.data.api.config.MockApi
import com.example.desafioinplanrio.data.api.service.RouteAPiService
import com.example.desafioinplanrio.data.api.config.RouteApi
import com.example.desafioinplanrio.data.api.service.MockAPiService
import com.example.desafioinplanrio.data.data_source.RouteLocalDataSource
import com.example.desafioinplanrio.data.data_source.RouteLocalDataSourceImpl
import com.example.desafioinplanrio.data.data_source.RouteRemoteDataSourceImpl
import com.example.desafioinplanrio.data.data_source.RouteRemoteDataSource
import com.example.desafioinplanrio.data.mapper.*
import com.example.desafioinplanrio.data.repositories.RouteRepositoryImpl
import com.example.desafioinplanrio.domain.repositories.RouteRepository
import com.example.desafioinplanrio.domain.usecases.GetHistorico
import com.example.desafioinplanrio.domain.usecases.GetRoute
import com.example.desafioinplanrio.presentation.pages.historico.HistoricoViewModel
import com.example.desafioinplanrio.presentation.pages.map.MapsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


//criado por arthur rodrigues

val modules = module{

    single<RouteAPiService> {  RouteApi.getRetrofit().create(RouteAPiService::class.java) }
    single <MockAPiService>{ MockApi.getRetrofit().create(MockAPiService::class.java) }

    single<RouteRemoteDataSource> {RouteRemoteDataSourceImpl(get(), get()) }
    single<RouteLocalDataSource> {RouteLocalDataSourceImpl()}

    single<RouteRepository> {RouteRepositoryImpl(get(), get(), get(), get(), get(), get(), get())}

    single {ResponseToSegmentoEntityMapper()}
    single {ResponseToViagemEntityMapper()}
    single {ResponseToEnderecoEntityMapper()}
    single {TripleToViagemMapper()}
    single { ViagemToTripleMapper()}
    single { MockResponseToSimpleViagemMapper() }

    single { GetRoute(get())}
    single { GetHistorico(get()) }
}

val viewModelModulo = module{
    viewModel { MapsViewModel( get() ) }
    viewModel { HistoricoViewModel( get() ) }
}