package com.example.desafioinplanrio.domain.repositories

import com.example.desafioinplanrio.data.database.entity.ViagemEntity
import com.example.desafioinplanrio.data.models.GoogleResponse
import com.example.desafioinplanrio.domain.entities.SimpleViagem
import com.example.desafioinplanrio.domain.entities.Viagem


//criado por arthur rodrigues

interface RouteRepository {

    suspend fun getRoute(origem : String, destino : String) : Viagem

    suspend fun salvaDadosRota(response : GoogleResponse)

    suspend fun getLocalRoute(origem: String, destino: String) : Viagem?

    suspend fun getHistorico() : List<SimpleViagem>
}