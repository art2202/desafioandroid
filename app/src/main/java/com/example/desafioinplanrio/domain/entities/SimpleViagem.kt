package com.example.desafioinplanrio.domain.entities

import com.google.gson.annotations.SerializedName


//criado por arthur rodrigues

class SimpleViagem(
    val origem : String,
    val destino : String,
    val dataSolicitacao : String,
    val distancia : String,
    val tempo : String
)