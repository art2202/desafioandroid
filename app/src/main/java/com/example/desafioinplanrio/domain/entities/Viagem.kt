package com.example.desafioinplanrio.domain.entities


//criado por arthur rodrigues

class Viagem(
    val dataViagem : String,
    val distancia : String,
    val duracao : String,
    val enderecoInicial : Endereco,
    val enderecoFinal : Endereco,
    val rota : List<Segmento>
)