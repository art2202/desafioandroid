package com.example.desafioinplanrio.domain.usecases

import com.example.desafioinplanrio.domain.entities.Viagem
import com.example.desafioinplanrio.domain.repositories.RouteRepository


//criado por arthur rodrigues

class GetRoute(private val routeRepository: RouteRepository){
    suspend operator fun invoke(origem : String, destino : String) = routeRepository.getRoute(origem, destino)
}