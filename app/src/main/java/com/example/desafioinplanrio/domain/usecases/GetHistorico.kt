package com.example.desafioinplanrio.domain.usecases

import com.example.desafioinplanrio.domain.entities.SimpleViagem
import com.example.desafioinplanrio.domain.repositories.RouteRepository


//criado por arthur rodrigues

class GetHistorico(private val routeRepository: RouteRepository) {
    suspend operator fun invoke() : List<SimpleViagem> = routeRepository.getHistorico()
}