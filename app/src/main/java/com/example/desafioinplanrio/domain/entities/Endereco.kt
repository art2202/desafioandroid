package com.example.desafioinplanrio.domain.entities

import com.google.android.gms.maps.model.LatLng


//criado por arthur rodrigues

class Endereco(
    val endereco : String,
    val latLng: LatLng
)